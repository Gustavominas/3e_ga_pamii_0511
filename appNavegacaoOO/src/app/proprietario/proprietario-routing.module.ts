import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProprietarioPage } from './proprietario.page';

const routes: Routes = [
  {
    path: '',
    component: ProprietarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProprietarioPageRoutingModule {}
