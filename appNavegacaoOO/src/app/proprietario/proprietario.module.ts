import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProprietarioPageRoutingModule } from './proprietario-routing.module';

import { ProprietarioPage } from './proprietario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProprietarioPageRoutingModule
  ],
  declarations: [ProprietarioPage]
})
export class ProprietarioPageModule {}
