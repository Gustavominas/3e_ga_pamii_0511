import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocadorPageRoutingModule } from './locador-routing.module';

import { LocadorPage } from './locador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocadorPageRoutingModule
  ],
  declarations: [LocadorPage]
})
export class LocadorPageModule {}
