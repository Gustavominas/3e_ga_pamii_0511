import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocadorPage } from './locador.page';

const routes: Routes = [
  {
    path: '',
    component: LocadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocadorPageRoutingModule {}
