import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  rotas = [
    {
    path: "/imovel",
    text: "Cadastrar Imóvel"
    },
    {
    path: "/imobiliaria",
    text: "Cadastrar Imobiliária"
    },
    {
        path: "/locador",
        text: "Cadastrar Locador"
    },
    {
      path: "/proprietario",
      text: "Cadastrar Proprietário"
    }
  ]

  constructor() {}

}
