export class Imobiliaria{
    private _nome: String;
    private _endereco: String;
    cidade: String;
    bairro: String;
    private _fone: String;
    site: String;

    constructor(){
        this._nome = "Gustavo";
        this._endereco = "12";
        this.cidade = "Itapeva";
        this.bairro = "Jardim Europa";
        this._fone = "";
        this.site = "";
    }

    public set nome(nome: String){
        this._nome = nome;
    }
    public get nome():String{
        return this._nome;
    }
}