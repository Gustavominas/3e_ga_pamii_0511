export class Proprietario{

    private _nome: string;
    private _telefone: string;
    private _dataNascimento: Date;
    private _sexo: string;
    private _fone: string;
    private _email: string;
    

    constructor(){
        this._nome = "";
        this._telefone = "";
        this._dataNascimento = new Date;
        this._sexo = "";
        this._fone = "";
        this._email = "";
    }
    public set nome(nome : string) {
        this._nome = nome;
    }
    public get nome():string {
        return this._nome
    }
    public set telefone (telefone : string) {
        this._telefone = telefone;
    }
    public get telefone ():string {
        return this._telefone
    }
    public set dataNascimento (dataNascimento : Date) {
        this._dataNascimento = dataNascimento;
    }
    public get dataNascimento():Date {
        return this._dataNascimento
    }
    public set sexo(sexo : string) {
        this._sexo = sexo;
    }
    public get sexo():string {
        return this.sexo
    }
    public set fone(fone : string) {
        this._fone = fone;
    }
    public get fone():string {
        return this.fone
    }
    public set email(email : string) {
        this.email = email;
    }
    public get email():string {
        return this.email
    }

}