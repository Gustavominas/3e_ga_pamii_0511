export class Locador{
    private _nome: string;
    private _telefone: string;
    private _datanascimento: Date;
    private _email: string;
    private _sexo : string;
    private _rendaMensal: number;
    private _nomeFiador: string;

    constructor(){
        this._nome = "nome";
        this._telefone = "";
        this._datanascimento = new Date;
        this._email = "";
        this._sexo = "";
        this._rendaMensal = 0;
        this._nomeFiador = "";
    }
    public set nome(nome : string) {
        this._nome = nome;
    }
    public get nome():string {
        return this._nome
    }

    public set telefone(telefone: string){
        this._telefone = telefone;
    }
    public get telefone(): string {
        return this._telefone
    }

    public set datanascimento(datanascimento: Date){
        this._datanascimento = datanascimento;
    }
    public get datanascimento(): Date {
        return this._datanascimento
    }

    public set email(email: string){
        this._email = email;
    }
    public get email(): string {
        return this._email
    }

    public set sexo(sexo: string){
        this._sexo = sexo;
    }
    public get sexo(): string {
        return this._sexo
    }

    public set rendaMensal(rendaMensal: number){
        this._rendaMensal = rendaMensal;
    }
    public get rendaMensal(): number {
        return this._rendaMensal
    }

    public set nomeFiador(nomeFiador: string){
        this._nomeFiador = nomeFiador;
    }
    public get nomeFiador(): string {
        return this._nomeFiador
    }
}

 

    