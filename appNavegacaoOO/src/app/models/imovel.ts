export class Imovel{
    endereco: String; //Rua e número
    bairro: String;
    cep: String;
    cidade: String;
    uf: String;
    qtdQuartos: number;
    qtdSalas: number;
    qtdBanheiros: number;
    qtdCozinhas: number;
    andares: number;
    complemento: String;
    valorVenal: number;
    valorLocacao: number;
    isLocavel: boolean;
    isVenal: boolean;
    situacao: String;

}