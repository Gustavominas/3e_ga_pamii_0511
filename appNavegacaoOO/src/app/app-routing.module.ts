import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'imovel',
    loadChildren: () => import('./imovel/imovel.module').then( m => m.ImovelPageModule)
  },
  {
    path: 'imobiliaria',
    loadChildren: () => import('./imobiliaria/imobiliaria.module').then( m => m.ImobiliariaPageModule)
  },
  {
    path: 'proprietario',
    loadChildren: () => import('./proprietario/proprietario.module').then( m => m.ProprietarioPageModule)
  },
  {
    path: 'locador',
    loadChildren: () => import('./locador/locador.module').then( m => m.LocadorPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
