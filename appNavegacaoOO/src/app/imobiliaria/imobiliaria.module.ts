import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImobiliariaPageRoutingModule } from './imobiliaria-routing.module';

import { ImobiliariaPage } from './imobiliaria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImobiliariaPageRoutingModule
  ],
  declarations: [ImobiliariaPage]
})
export class ImobiliariaPageModule {}
