import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImobiliariaPage } from './imobiliaria.page';

const routes: Routes = [
  {
    path: '',
    component: ImobiliariaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImobiliariaPageRoutingModule {}
